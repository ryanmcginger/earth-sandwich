# Earth Sandwich

See the website here: [93% Earth Sandwich](https://earth-sandwich-ryanmcginger-dad9ff555b0afe76e89223e68589ad60fbd.gitlab.io/)

env setup:

```python
conda create -n earth-sandwich -c conda-forge -y python=3.11 jupyterlab descartes geopandas folium matplotlib
```
